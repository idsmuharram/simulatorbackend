package shooters

import (
	"context"
	"time"

	"gorm.io/gorm"
)

type Domain struct {
	ID          int
	ShooterName string
	RoundsFired int
	Hits        int
	Missed      int
	Score       int
	Result      string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt
}

type Usecase interface {
	FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error)
	GetAll(ctx context.Context) ([]Domain, error)
	GetByID(ctx context.Context, shooterId int) (Domain, error)
	Store(ctx context.Context, shooterDomain *Domain) (Domain, error)
}

type Repository interface {
	FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error)
	Find(ctx context.Context) ([]Domain, error)
	GetByID(ctx context.Context, shooterId int) (Domain, error)
	Store(ctx context.Context, shootersDomain *Domain) (Domain, error)
}
