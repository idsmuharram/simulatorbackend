package shooters

import (
	"context"
	usecase "shootsimulator/usecase"
	"time"
)

type shooterUsecase struct {
	shooterRepository Repository
	contextTimeout    time.Duration
}

func NewshooterUsecase(timeout time.Duration, cr Repository) Usecase {
	return &shooterUsecase{
		contextTimeout:    timeout,
		shooterRepository: cr,
	}
}

func (uc *shooterUsecase) FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	if page <= 0 {
		page = 1
	}
	if perpage <= 0 {
		perpage = 25
	}

	res, total, err := uc.shooterRepository.FindAll(ctx, page, perpage)
	if err != nil {
		return []Domain{}, 0, err
	}

	return res, total, nil
}

func (uc *shooterUsecase) GetAll(ctx context.Context) ([]Domain, error) {
	resp, err := uc.shooterRepository.Find(ctx)
	if err != nil {
		return []Domain{}, err
	}
	return resp, nil
}

func (uc *shooterUsecase) GetByID(ctx context.Context, shooterID int) (Domain, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	if shooterID <= 0 {
		return Domain{}, usecase.ErrNotFound
	}
	res, err := uc.shooterRepository.GetByID(ctx, shooterID)
	if err != nil {
		return Domain{}, err
	}

	return res, nil
}

func (uc *shooterUsecase) Store(ctx context.Context, shooterDomain *Domain) (Domain, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	result, err := uc.shooterRepository.Store(ctx, shooterDomain)
	if err != nil {
		return Domain{}, err
	}

	return result, nil
}
