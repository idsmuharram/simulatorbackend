package instructors

import (
	"context"
	usecase "shootsimulator/usecase"
	"time"
)

type instructorUsecase struct {
	instructorRepository Repository
	contextTimeout       time.Duration
}

func NewInstructorUsecase(timeout time.Duration, cr Repository) Usecase {
	return &instructorUsecase{
		contextTimeout:       timeout,
		instructorRepository: cr,
	}
}

func (uc *instructorUsecase) FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	if page <= 0 {
		page = 1
	}
	if perpage <= 0 {
		perpage = 25
	}

	res, total, err := uc.instructorRepository.FindAll(ctx, page, perpage)
	if err != nil {
		return []Domain{}, 0, err
	}

	return res, total, nil
}

func (uc *instructorUsecase) GetAll(ctx context.Context) ([]Domain, error) {
	resp, err := uc.instructorRepository.Find(ctx)
	if err != nil {
		return []Domain{}, err
	}
	return resp, nil
}

func (uc *instructorUsecase) GetByID(ctx context.Context, instructorID int) (Domain, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	if instructorID <= 0 {
		return Domain{}, usecase.ErrNotFound
	}
	res, err := uc.instructorRepository.GetByID(ctx, instructorID)
	if err != nil {
		return Domain{}, err
	}

	return res, nil
}

func (uc *instructorUsecase) Store(ctx context.Context, instructorDomain *Domain) (Domain, error) {
	ctx, cancel := context.WithTimeout(ctx, uc.contextTimeout)
	defer cancel()

	result, err := uc.instructorRepository.Store(ctx, instructorDomain)
	if err != nil {
		return Domain{}, err
	}

	return result, nil
}
