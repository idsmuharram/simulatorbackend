package instructors

import (
	"context"
	"time"

	"gorm.io/gorm"
)

type Domain struct {
	ID             int
	InstructorName string
	Pangkat        string
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      gorm.DeletedAt
}

type Usecase interface {
	FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error)
	GetAll(ctx context.Context) ([]Domain, error)
	GetByID(ctx context.Context, instructorId int) (Domain, error)
	Store(ctx context.Context, instructorDomain *Domain) (Domain, error)
}

type Repository interface {
	FindAll(ctx context.Context, page, perpage int) ([]Domain, int, error)
	Find(ctx context.Context) ([]Domain, error)
	GetByID(ctx context.Context, instructorId int) (Domain, error)
	Store(ctx context.Context, instructorsDomain *Domain) (Domain, error)
}
