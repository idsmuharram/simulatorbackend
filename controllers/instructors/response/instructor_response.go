package response

import (
	"shootsimulator/usecase/instructors"
	"time"

	"gorm.io/gorm"
)

type Instructor struct {
	ID             int            `json:"id"`
	InstructorName string         `json:"instructor_name"`
	Pangkat        string         `json:"pangkat"`
	CreatedAt      time.Time      `json:"created_at"`
	UpdatedAt      time.Time      `json:"updated_at"`
	DeletedAt      gorm.DeletedAt `json:"deleted_at"`
}

func FromDomain(domain instructors.Domain) Instructor {
	return Instructor{
		ID:             domain.ID,
		InstructorName: domain.InstructorName,
		Pangkat:        domain.Pangkat,
		CreatedAt:      domain.CreatedAt,
		UpdatedAt:      domain.UpdatedAt,
		DeletedAt:      domain.DeletedAt,
	}
}
