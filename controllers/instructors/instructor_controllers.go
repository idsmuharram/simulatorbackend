package instructors

import (
	"net/http"
	"shootsimulator/controllers/instructors/request"
	"shootsimulator/controllers/instructors/response"
	"shootsimulator/usecase/instructors"
	"strconv"

	controller "shootsimulator/controllers"

	echo "github.com/labstack/echo/v4"
)

type InstructorController struct {
	instructorUsecase instructors.Usecase
}

func NewInstructorController(cu instructors.Usecase) *InstructorController {
	return &InstructorController{
		instructorUsecase: cu,
	}
}

func (ctrl *InstructorController) SelectAll(c echo.Context) error {
	ctx := c.Request().Context()

	resp, err := ctrl.instructorUsecase.GetAll(ctx)
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	responseController := []response.Instructor{}
	for _, value := range resp {
		responseController = append(responseController, response.FromDomain(value))
	}

	return controller.NewSuccessResponse(c, responseController)
}

func (ctrl *InstructorController) FindAll(c echo.Context) error {
	ctx := c.Request().Context()
	page, _ := strconv.Atoi(c.QueryParam("page"))
	limit, _ := strconv.Atoi(c.QueryParam("limit"))

	if page <= 0 {
		page = 1
	}
	if limit <= 0 || limit > 50 {
		limit = 10
	}

	resp, total, err := ctrl.instructorUsecase.FindAll(ctx, page, limit)
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	responseController := []response.Instructor{}
	for _, value := range resp {
		responseController = append(responseController, response.FromDomain(value))
	}

	pagination := controller.PaginationRes(page, total, limit)
	return controller.NewSuccessResponsePagination(c, responseController, pagination)
}

func (ctrl *InstructorController) FindById(c echo.Context) error {
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	instructor, err := ctrl.instructorUsecase.GetByID(ctx, id)

	if err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	return controller.NewSuccessResponse(c, response.FromDomain(instructor))
}

func (ctrl *InstructorController) Store(c echo.Context) error {
	ctx := c.Request().Context()

	req := request.Instructors{}
	if err := c.Bind(&req); err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	resp, err := ctrl.instructorUsecase.Store(ctx, req.ToDomain())
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	return controller.NewSuccessResponse(c, response.FromDomain(resp))
}
