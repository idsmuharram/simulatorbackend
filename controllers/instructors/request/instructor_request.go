package request

import (
	"shootsimulator/usecase/instructors"
)

type Instructors struct {
	InstructorName string `json:"instructor_name"`
	Pangkat        string `json:"pangkat"`
}

func (req *Instructors) ToDomain() *instructors.Domain {
	return &instructors.Domain{
		InstructorName: req.InstructorName,
		Pangkat:        req.Pangkat,
	}
}
