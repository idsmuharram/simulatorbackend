package response

import (
	"shootsimulator/usecase/shooters"
	"time"

	"gorm.io/gorm"
)

type Shooter struct {
	ID          int            `json:"id"`
	ShooterName string         `json:"shooter_name"`
	RoundsFired int            `json:"rounds_fired"`
	Hits        int            `json:"hits"`
	Missed      int            `json:"missed"`
	Score       int            `json:"score"`
	Result      string         `json:"result"`
	CreatedAt   time.Time      `json:"created_at"`
	UpdatedAt   time.Time      `json:"updated_at"`
	DeletedAt   gorm.DeletedAt `json:"deleted_at"`
}

func FromDomain(domain shooters.Domain) Shooter {
	return Shooter{
		ID:          domain.ID,
		ShooterName: domain.ShooterName,
		RoundsFired: domain.RoundsFired,
		Hits:        domain.Hits,
		Missed:      domain.Missed,
		Score:       domain.Score,
		Result:      domain.Result,
		CreatedAt:   domain.CreatedAt,
		UpdatedAt:   domain.UpdatedAt,
		DeletedAt:   domain.DeletedAt,
	}
}
