package shooters

import (
	"net/http"
	"shootsimulator/controllers/shooters/request"
	"shootsimulator/controllers/shooters/response"
	"shootsimulator/usecase/shooters"
	"strconv"

	controller "shootsimulator/controllers"

	echo "github.com/labstack/echo/v4"
)

type ShooterController struct {
	shooterUsecase shooters.Usecase
}

func NewShooterController(cu shooters.Usecase) *ShooterController {
	return &ShooterController{
		shooterUsecase: cu,
	}
}

func (ctrl *ShooterController) SelectAll(c echo.Context) error {
	ctx := c.Request().Context()

	resp, err := ctrl.shooterUsecase.GetAll(ctx)
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	responseController := []response.Shooter{}
	for _, value := range resp {
		responseController = append(responseController, response.FromDomain(value))
	}

	return controller.NewSuccessResponse(c, responseController)
}

func (ctrl *ShooterController) FindAll(c echo.Context) error {
	ctx := c.Request().Context()
	page, _ := strconv.Atoi(c.QueryParam("page"))
	limit, _ := strconv.Atoi(c.QueryParam("limit"))

	if page <= 0 {
		page = 1
	}
	if limit <= 0 || limit > 50 {
		limit = 10
	}

	resp, total, err := ctrl.shooterUsecase.FindAll(ctx, page, limit)
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	responseController := []response.Shooter{}
	for _, value := range resp {
		responseController = append(responseController, response.FromDomain(value))
	}

	pagination := controller.PaginationRes(page, total, limit)
	return controller.NewSuccessResponsePagination(c, responseController, pagination)
}

func (ctrl *ShooterController) FindById(c echo.Context) error {
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))

	if err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	shooter, err := ctrl.shooterUsecase.GetByID(ctx, id)

	if err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	return controller.NewSuccessResponse(c, response.FromDomain(shooter))
}

func (ctrl *ShooterController) Store(c echo.Context) error {
	ctx := c.Request().Context()

	req := request.Shooters{}
	if err := c.Bind(&req); err != nil {
		return controller.NewErrorResponse(c, http.StatusBadRequest, err)
	}

	resp, err := ctrl.shooterUsecase.Store(ctx, req.ToDomain())
	if err != nil {
		return controller.NewErrorResponse(c, http.StatusInternalServerError, err)
	}

	return controller.NewSuccessResponse(c, response.FromDomain(resp))
}
