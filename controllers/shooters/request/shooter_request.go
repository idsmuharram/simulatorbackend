package request

import "shootsimulator/usecase/shooters"

type Shooters struct {
	ShooterName string `json:"shooter_name"`
	RoundsFired int    `json:"rounds_fired"`
	Hits        int    `json:"hits"`
	Missed      int    `json:"missed"`
	Score       int    `json:"score"`
	Result      string `json:"result"`
}

func (req *Shooters) ToDomain() *shooters.Domain {
	return &shooters.Domain{
		ShooterName: req.ShooterName,
		RoundsFired: req.RoundsFired,
		Hits:        req.Hits,
		Missed:      req.Missed,
		Score:       req.Score,
		Result:      req.Result,
	}
}
