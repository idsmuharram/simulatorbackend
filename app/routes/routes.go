package routes

import (
	"shootsimulator/controllers/instructors"
	"shootsimulator/controllers/shooters"

	"github.com/labstack/echo/v4"
)

type ControllerList struct {
	InstructorsController instructors.InstructorController
	ShootersController shooters.ShooterController
}

func (cl *ControllerList) RouteRegister(e *echo.Echo) {
	r := e.Group("/api")

	//instructors ...
	instructor := r.Group("/instructors")

	instructor.GET("/select", cl.InstructorsController.SelectAll)
	instructor.GET("", cl.InstructorsController.FindAll)
	instructor.GET("/id/:id", cl.InstructorsController.FindById)
	instructor.POST("", cl.InstructorsController.Store)

	//shooters ...
	shooter := r.Group("/shooters")

	shooter.GET("/select", cl.ShooterController.SelectAll)
	shooter.GET("", cl.ShooterController.FindAll)
	shooter.GET("/id/:id", cl.ShooterController.FindById)
	shooter.POST("", cl.ShooterController.Store)

}
