package main

import (
	_routes "shootsimulator/app/routes"

	_instructorsController "shootsimulator/controllers/instructors"
	_instructorsRepo "shootsimulator/repository/databases/instructors"
	_instructorsUsecase "shootsimulator/usecase/instructors"

	_shootersController "shootsimulator/controllers/shooters"
	_shootersRepo "shootsimulator/repository/databases/shooters"
	_shootersUsecase "shootsimulator/usecase/shooters"

	_dbDriver "shootsimulator/repository/mysql"

	"log"
	"time"

	echo "github.com/labstack/echo/v4"
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigFile(`config.json`)
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	if viper.GetBool(`debug`) {
		log.Println("Service RUN on DEBUG mode")
	}
}

func main() {
	configDB := _dbDriver.ConfigDB{
		DB_Username: viper.GetString(`database.user`),
		DB_Password: viper.GetString(`database.pass`),
		DB_Host:     viper.GetString(`database.host`),
		DB_Port:     viper.GetString(`database.port`),
		DB_Database: viper.GetString(`database.name`),
	}
	db := configDB.InitDB()

	timeoutContext := time.Duration(viper.GetInt("context.timeout")) * time.Second

	e := echo.New()

	// Instructors ...
	instructorsRepo := _instructorsRepo.NewMySQLInstructorRepository(db)
	instructorsUsecase := _instructorsUsecase.NewInstructorUsecase(timeoutContext, instructorsRepo)
	instructorsCtrl := _instructorsController.NewInstructorController(instructorsUsecase)

	// Shooters ...
	shootersRepo := _shootersRepo.NewMySQLShooterRepository(db)
	shootersUsecase := _shootersUsecase.NewShooterUsecase(timeoutContext, shootersRepo)
	shootersCtrl := _shootersController.NewShooterController(shootersUsecase)

	routesInit := _routes.ControllerList{
		InstructorsController: *instructorsCtrl,
		ShootersController: *shootersCtrl,
	}
	routesInit.RouteRegister(e)

	log.Fatal(e.Start(viper.GetString("server.address")))
}
