package instructors

import (
	"context"
	"shootsimulator/usecase/instructors"

	"gorm.io/gorm"
)

type mysqlInstructorsRepository struct {
	DB *gorm.DB
}

func NewMySQLInstructorRepository(conn *gorm.DB) instructors.Repository {
	return &mysqlInstructorsRepository{
		DB: conn,
	}
}

func (repo *mysqlInstructorsRepository) FindAll(ctx context.Context, page, perpage int) ([]instructors.Domain, int, error) {
	rec := []Instructors{}

	offset := (page - 1) * perpage
	err := repo.DB.Find(&rec).Offset(offset).Limit(perpage).Error
	if err != nil {
		return []instructors.Domain{}, 0, err
	}

	var totalData int64
	err = repo.DB.Model(&rec).Count(&totalData).Error
	if err != nil {
		return []instructors.Domain{}, 0, err
	}

	var domainInstructor []instructors.Domain
	for _, value := range rec {
		domainInstructor = append(domainInstructor, value.toDomain())
	}
	return domainInstructor, int(totalData), nil
}

func (repo *mysqlInstructorsRepository) Find(ctx context.Context) ([]instructors.Domain, error) {
	rec := []Instructors{}

	repo.DB.Find(&rec)
	instructorDomain := []instructors.Domain{}
	for _, value := range rec {
		instructorDomain = append(instructorDomain, value.toDomain())
	}

	return instructorDomain, nil
}

func (repo *mysqlInstructorsRepository) GetByID(ctx context.Context, instructorId int) (instructors.Domain, error) {
	rec := Instructors{}
	err := repo.DB.Where("id = ?", instructorId).First(&rec).Error
	if err != nil {
		return instructors.Domain{}, err
	}
	return rec.toDomain(), nil
}

func (repo *mysqlInstructorsRepository) Store(ctx context.Context, instructorsDomain *instructors.Domain) (instructors.Domain, error) {
	rec := fromDomain(*instructorsDomain)

	result := repo.DB.Create(&rec)
	if result.Error != nil {
		return instructors.Domain{}, result.Error
	}

	return rec.toDomain(), nil
}
