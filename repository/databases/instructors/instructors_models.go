package instructors

import (
	"shootsimulator/usecase/instructors"

	"time"

	"gorm.io/gorm"
)

type Instructors struct {
	ID             int
	InstructorName string
	Pangkat        string
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      gorm.DeletedAt
}

func fromDomain(instructorsDomain instructors.Domain) *Instructors {
	return &Instructors{
		ID:             instructorsDomain.ID,
		InstructorName: instructorsDomain.InstructorName,
		Pangkat:        instructorsDomain.Pangkat,
		CreatedAt:      instructorsDomain.CreatedAt,
		UpdatedAt:      instructorsDomain.UpdatedAt,
		DeletedAt:      instructorsDomain.DeletedAt,
	}
}

func (rec *Instructors) toDomain() instructors.Domain {
	return instructors.Domain{
		ID:             rec.ID,
		InstructorName: rec.InstructorName,
		Pangkat:        rec.Pangkat,
		CreatedAt:      rec.CreatedAt,
		UpdatedAt:      rec.UpdatedAt,
		DeletedAt:      rec.DeletedAt,
	}
}
