package shooters

import (
	"shootsimulator/usecase/shooters"

	"time"

	"gorm.io/gorm"
)

type Shooters struct {
	ID          int
	ShooterName string
	RoundsFired int
	Hits        int
	Missed      int
	Score       int
	Result      string
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt
}

func fromDomain(shootersDomain shooters.Domain) *Shooters {
	return &Shooters{
		ID:          shootersDomain.ID,
		ShooterName: shootersDomain.ShooterName,
		RoundsFired: shootersDomain.RoundsFired,
		Hits:        shootersDomain.Hits,
		Missed:      shootersDomain.Missed,
		Score:       shootersDomain.Score,
		Result:      shootersDomain.Result,
		CreatedAt:   shootersDomain.CreatedAt,
		UpdatedAt:   shootersDomain.UpdatedAt,
		DeletedAt:   shootersDomain.DeletedAt,
	}
}

func (rec *shooters) toDomain() shooters.Domain {
	return shooters.Domain{
		ID:          rec.ID,
		ShooterName: rec.ShooterName,
		RoundsFired: rec.RoundsFired,
		Hits:        rec.Hits,
		Missed:      rec.Missed,
		Score:       rec.Score,
		Result:      rec.Result,
		CreatedAt:   rec.CreatedAt,
		UpdatedAt:   rec.UpdatedAt,
		DeletedAt:   rec.DeletedAt,
	}
}
