package shooters

import (
	"context"
	"shootsimulator/usecase/shooters"

	"gorm.io/gorm"
)

type mysqlShootersRepository struct {
	DB *gorm.DB
}

func NewMySQLShooterRepository(conn *gorm.DB) shooters.Repository {
	return &mysqlShootersRepository{
		DB: conn,
	}
}

func (repo *mysqlShootersRepository) FindAll(ctx context.Context, page, perpage int) ([]shooters.Domain, int, error) {
	rec := []Shooters{}

	offset := (page - 1) * perpage
	err := repo.DB.Find(&rec).Offset(offset).Limit(perpage).Error
	if err != nil {
		return []shooters.Domain{}, 0, err
	}

	var totalData int64
	err = repo.DB.Model(&rec).Count(&totalData).Error
	if err != nil {
		return []shooters.Domain{}, 0, err
	}

	var domainShooter []shooters.Domain
	for _, value := range rec {
		domainShooter = append(domainShooter, value.toDomain())
	}
	return domainShooter, int(totalData), nil
}

func (repo *mysqlShootersRepository) Find(ctx context.Context) ([]shooters.Domain, error) {
	rec := []Shooters{}

	repo.DB.Find(&rec)
	shooterDomain := []shooters.Domain{}
	for _, value := range rec {
		shooterDomain = append(shooterDomain, value.toDomain())
	}

	return shooterDomain, nil
}

func (repo *mysqlShootersRepository) GetByID(ctx context.Context, shooterId int) (shooters.Domain, error) {
	rec := Shooters{}
	err := repo.DB.Where("id = ?", shooterId).First(&rec).Error
	if err != nil {
		return shooters.Domain{}, err
	}
	return rec.toDomain(), nil
}

func (repo *mysqlShootersRepository) Store(ctx context.Context, shootersDomain *shooters.Domain) (shooters.Domain, error) {
	rec := fromDomain(*shootersDomain)

	result := repo.DB.Create(&rec)
	if result.Error != nil {
		return shooters.Domain{}, result.Error
	}

	return rec.toDomain(), nil
}
